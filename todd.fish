#!/usr/bin/fish

set _dir "$HOME/.todd"
set _file "$_dir/todd.txt"
set _tmp_file "$_dir/todd.tmp.txt"

if not test -d $_dir
    mkdir $_dir && touch $_file
end

function _help
    echo "▄▄▄▄▄      ·▄▄▄▄  ·▄▄▄▄  
•██  ▪     ██▪ ██ ██▪ ██ 
 ▐█.▪ ▄█▀▄ ▐█· ▐█▌▐█· ▐█▌
 ▐█▌·▐█▌.▐▌██. ██ ██. ██ 
 ▀▀▀  ▀█▄▀▪▀▀▀▀▀• ▀▀▀▀▀•"\n

    echo "Usage"
    echo "Basics: c, create: Create a new todo."
    echo "        l, list: List all todos."
    echo "        r, remove: Remove a todo based on his index."
    echo "        --delete-all-todos: Delete all todos."\n

    echo "Tagging: c, chill: Create a chill todo. [🍄]"
    echo "         i, idea: Create an idea todo. [💡]"
    echo "         n, normal: Create a normal todo. [⌛️]"
    echo "         u, urgent: Create an urgent todo. [🚨]"\n

    echo "Examples"
    echo "Create an urgent todo:           todd c \"This is urgent !\" u"
    echo "List all todos and show numbers: todd l num"
    echo "List all urgent todos:           todd l u"
    echo "Remove the first todo:           todd r 1"
    echo "Delete all todos:                todd --delete-all-todos"
end

switch $argv[1]
    case c create
        switch $argv[3]
            case c chill
                echo "🍄 $argv[2]" >>$_file
            case i idea
                echo "💡 $argv[2]" >>$_file
            case n normal
                echo "⌛️ $argv[2]" >>$_file
            case u urgent
                echo "🚨 $argv[2]" >>$_file
            case "*"
                echo "⌛️ $argv[2]" >>$_file
        end
        cat $_file
    case h help
        _help
    case l list
        switch $argv[2]
            case c chill
                sed -n '/^🍄/p' $_file
            case i idea
                sed -n '/^💡/p' $_file
            case u urgent
                sed -n '/^🚨/p' $_file
            case n normal
                sed -n '/^⌛️/p' $_file
            case num number
                cat --number $_file
            case "*"
                cat $_file
        end
    case r remove
        cp $_file $_tmp_file
        sed "$argv[2]d" $_tmp_file | tee $_file
        rm $_tmp_file
    case --delete-all-todos
        rm $_file && touch $file
        echo "$_file was deleted."
    case '*'
        _help
end