# Todd

- [Todd](#todd)
    - [Buit with](#buit-with)
    - [Version](#version)
  - [Getting started](#getting-started)
    - [Download](#download)
    - [Installation](#installation)
      - [Installation tips](#installation-tips)
    - [Running](#running)
  - [Usage](#usage)
    - [Basics](#basics)
    - [Tagging](#tagging)
  - [Examples](#examples)
  - [Authors](#authors)
  - [License](#license)

### Buit with

- [Fish](https://fishshell.com/) - The language used.

### Version

**0.1.0**

## Getting started

### Download

`git clone https://gitlab.com/thelittlebigbot/todd.git`

### Installation

```fish
set todd_dir "/folder/path/todd"
alias todd "$todd_dir/todd.fish"
```

#### Installation tips

```fish
alias tdc "todd create"
alias tdl "todd list"
alias tdr "todd remove"
```

### Running

`todd`

## Usage

### Basics

- `c, create`: Create a new todo.
- `l, list`: List all todos.
- `r, remove`: Remove a todo based on his index.
- `--delete-all-todos`: Delete all todos.

### Tagging

_If you do not enter a tag, the normal tag will be applied._

- `c, chill`: Create a chill todo. [🍄]
- `i, idea`: Create an idea todo. [💡]
- `n, normal`: Create a nomal todo. [⌛️]
- `u, urgent`: Create an urgent todo. [🚨]

## Examples

- Create an urgent todo: `todd c "This is urgent !" u`
- List all todos and show numbers: `todd l num`
- List all urgent todos: `todd l u`
- Remove the first todo: `todd r 1`
- Delete all todos: `todd --delete-all-todos`

## Authors

- **Alexandre Figueiredo** - _Initial work_ - [thelittlebigbot](mailto:thelittlebigbot@gmail.com)

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.
